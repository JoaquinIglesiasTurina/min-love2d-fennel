# Mac OS X
init_module "Mac OS X" "osx" "M"
OPTIONS="M"
LONG_OPTIONS=""


IDENTITY=$(echo $TITLE | sed -e 's/[^-a-zA-Z0-9_]/-/g' | tr '[:upper:]' '[:lower:]')

if [[ $LOVE_VERSION == "development" ]]; then
  echo "Release under development version is not supported for mac"
  exit_module "version"
fi

if [[ -z $AUTHOR ]]; then
    exit_module "options" "Missing maintainer's name. Use -a or --Mauthor."
fi
if [[ -z $GAME_VERSION ]]; then
    GAME_VERSION="$LOVE_VERSION"
fi

if [[ -n $ICON ]]; then
    if [[ -d $ICON ]]; then
        for file in $ICON/*.icns; do
            if [[ -f $file ]]; then
                ICON="$file"
                break
            else
                found=false
            fi
        done
    fi
    if [[ $found == false || ! -f $ICON ]]; then
        >&2 echo "OS X icon was not found in ${ICON}."
        icon=Love.icns
        ICON=
    else
        icon="${IDENTITY}.icns"
    fi
fi


create_love_file 9
cd "$RELEASE_DIR"


## MacOS ##
if [[ ! -f "$CACHE_DIR/love-$LOVE_VERSION-macos.zip" ]]; then
    curl -L -C - -o $CACHE_DIR/love-$LOVE_VERSION-macos.zip https://github.com/love2d/love/releases/download/$LOVE_VERSION/love-$LOVE_VERSION-macos.zip
fi
unzip -qq "$CACHE_DIR/love-$LOVE_VERSION-macos.zip"

rm -rf "$TITLE-macos.zip" 2> /dev/null
mv love.app "${TITLE}.app"
cp "$LOVE_FILE" "${TITLE}.app/Contents/Resources"
if [[ -n $ICON ]]; then
    cd "$PROJECT_DIR"
    cp "$ICON" "$RELEASE_DIR/$icon"
    cd "$RELEASE_DIR"
    mv "$icon" "${TITLE}.app/Contents/Resources"
fi

sed -i.bak -e '/<key>UTExportedTypeDeclarations<\/key>/,/^\t<\/array>/d' \
    -e "s/>org.love2d.love</>org.${AUTHOR}.$IDENTITY</" \
    -e "s/$LOVE_VERSION/$GAME_VERSION/" \
    -e "s/Love.icns/$icon/" \
    -e "s/>LÖVE</>$TITLE</" \
    "${TITLE}.app/Contents/Info.plist"
rm "${TITLE}.app/Contents/Info.plist.bak"

zip -9 -qyr "${TITLE}-macos.zip" "${TITLE}.app"
rm -rf love-$LOVE_VERSION-macos.zip "${TITLE}.app" __MACOSX

exit_module
