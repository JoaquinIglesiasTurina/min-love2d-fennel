#!/bin/bash

help="new-game.sh: \n
    A tool for duplicating min-love2d-fennel. \n\
\n\
usage: \n\
    ./new-game.sh [project-name] [opts]\n\
\n
opts:\n
  -o= (--output-directory=) Target parent directory. Defaults to PWD\n
  -l= (--layout=) Set the layout. Defaults to minimal layout.\n
  -f (--force) overwrite the target directory if it exists\n
  --development Use love's development version\n
  -h  (--help) Display this text\n
\n
eg: \n\
./new-game.sh sample-game -o=../ -f
"

name=$1
output_dir=$(pwd)
gethelp=false
location=$(dirname "$0")
force=false
layout="clone"

for i in "$@"
do
case $i in
    -o=*|--output-directory=*)
    output_dir="${i#*=}"
    ;;
    -l=*|--layout=*)
    layout="${i#*=}"
    ;;    
    -f|--force)
    force=true
    ;;    
    -h|--help)
    gethelp=true
    ;;
    --development)
      development=true
      ;;
    *)
            # unknown option
    ;;
esac
done

if [[ $layout == "seperate-source"  &&  $development ]]; then
  echo "Seperate source under development branch is not supported"
  exit 1
fi

if [ ! -d $output_dir ]; then
    echo "output directory not found!"
    echo $output_dir
    exit 1
fi

if [ ! -d $location/$layout ]; then
    echo "\"$layout\" layout not found!"
    echo "Valid built in layouts are:"
    echo "clone"
    echo "seperate-source"
    exit 1
fi

target_dir=$output_dir/$name

if [ -d $target_dir ]; then
    if [ $force = true ]; then
        echo "Overwriting $target_dir."
        rm -rf $target_dir
    else
        echo "target directory already exists! Use -f or --force to overwrite this directory."
        echo $target_dir
        exit 1
    fi
fi

copy-dir (){
    mkdir $target_dir
    ./$location/$layout/update-layout.sh $target_dir
    if [ $development ]; then
      echo "Development duplication"
      echo $layout
      cp -rf ./$location/development/conf.lua $target_dir
      cp -rf ./$location/development/mode-intro.fnl $target_dir
      awk '{sub(/LOVE_VERSION=11.4/,"LOVE_VERSION=\"development\"")}1' $target_dir/makefile > $target_dir/devel_makefile
      cp -rf $target_dir/devel_makefile $target_dir/makefile
      rm -rf $target_dir/devel_makefile
      awk '{sub(/LOVE_DEF_VERSION=0.11.4/,"LOVE_DEF_VERSION=\"development\"")}1' \
          $target_dir/buildtools/love-release.sh > $target_dir/buildtools/devel_love-release.sh
      cp -rf $target_dir/buildtools/devel_love-release.sh $target_dir/buildtools/love-release.sh
      rm -rf $target_dir/buildtools/devel_love-release.sh
      
    fi
    rm -rf $location/.duplicate
    rm -rf $location/.git
}

if [ "$layout" == "seperate-source" ] && [ $development ]; then
  echo "Development only works for non seperate source"
  exit 1
fi

if [ $gethelp = true ]; then
    echo -e $help
else
    copy-dir
fi
   
