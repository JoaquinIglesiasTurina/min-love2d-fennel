(import-macros {: incf} :sample-macros)
(local https (require :https))

(var counter 0)
(var time 0)

(love.graphics.setNewFont 30)

(local (major minor revision) (love.getVersion))
(local headers {:User-Agent "fun-stuffs"})
(local (code github-zen _) (https.request "https://api.github.com/zen"
                                          {:method "get"
                                           :headers headers}))

{:draw (fn draw [message]
         (local (w h _flags) (love.window.getMode))
         (love.graphics.printf
          (: "Love Version: %s.%s.%s"
             :format  major minor revision) 0 10 w :center)
         (love.graphics.printf
          (: "This window should close in %0.1f seconds"
             :format (math.max 0 (- 3 time)))
          0 (- (/ h 2) 15) w :center)
         (when (= 200 code)
           (do
             (love.graphics.printf
              "Here is some zen downloaded via https:"
              0 (* 2.7 (/ h 4)) w :center)
             (love.graphics.printf
              github-zen
              0 (* 3 (/ h 4)) w :center))))
 :update (fn update [dt set-mode]
           (if (< counter (* 3 65535))
               (set counter (+ counter 1))
               (set counter 0))
           (incf time dt)
           (when (> time 3)
             (love.event.quit)))
 :keypressed (fn keypressed [key set-mode]
               (love.event.quit))}
