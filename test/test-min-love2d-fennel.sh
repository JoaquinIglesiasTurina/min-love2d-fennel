test_windows() {
  echo "Testing windows"
  make windows
}
test_mac() {
  echo "Testing mac"
  make mac
}
test_linux() {
  echo "Testing Linux"
  make linux
  ./releases/change-me-0.1.0-x86_64.AppImage
}

echo "Testing Stable Version"

./min-love2d-fennel/.duplicate/new-game.sh test-min-love2d -f
cd test-min-love2d

test_linux
test_windows
test_mac

cd ..
echo "Testing Development Version"
echo "Next new game configuration is not supported"
./min-love2d-fennel/.duplicate/new-game.sh test-min-love2d \
                                           -l="seperate-source" --development

echo "Creating new game configuration"
./min-love2d-fennel/.duplicate/new-game.sh test-min-love2d -f --development
cd test-min-love2d
test_linux
test_windows

echo "Development release for mac is not supported"
test_mac






